package com.bjpowernode.pay.config;

import java.io.FileWriter;
import java.io.IOException;

/**
 * ClassName:AlipayConfig
 * Package:com.alipay.config
 * Description:
 *
 * @date:2019/6/21 10:56
 * @author:guoxin
 */
public class AlipayConfig {
    //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2016082000291278";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCbXupWcHI5ohzHUW+6ef7X8nbD/JvKnmaq9FzsVaF5cHc4iDEur+Y+83QIjcKXWFcHwoYekgzArpuZIa4xdJa+d7xRCBcRtCvtJgp3v/bhsMQwEY3ETIm/VSMcxkUX8T9qAuEOLuTmCqX7bPaml3xlLuXvLTqQOD7lGaaH8R2iJV2unyqheuAimKhZWtMBpF4i67Tx1GGuxJYp0EQTH98V6mn/brUByMuP+tttkgzKaSXfsR1MtnxGI4Cl8WLL0FIWIY16ErvDGXWkrjn/LXBLERcECkhVKAQGPqcAhGplaNZ+t5b6RiRChAwZS9lYCyTKYse+69i/upeEt2B7xev7AgMBAAECggEBAIkilKmo5FkUSlxu97Pa++G047kG3KdReoMH3oDG8BhU+RXPt8FihlIn5dGq9gdJdL+npWOUO2dSdooJnUuPgYIqAvBuuJXqth7CRdj+ebUopXFb/Qo3GBnl1nEOW2vPhs5WLjirkWOuIGSz/1x5NtXEODUMcbVbv7/NDRzN5/71nZPFynGkf6oc48UvXaEWARwr7JD++0pa16d2DJT5Z0WWv8d654ahopnWD7z+isaP9Gp3JE6JZypxecr9UZc+x/HgeaRGPcauA1Aq2NkeM3mBqkKN7XD/9ifZh+PkmE+9VPDy083KCydLIS0QKEm/z1Wj5RmhzMz4oKfMXwCPvLECgYEA+FW4vG4NfUgbxYByCIB0tEJEKRa/lMKNit/62B20Qna6q/fqWohIRN+kc1jfkgAcadXfdyRNO57xC0QIIGE/WoX04GOXP2HrnohsaKmRcsTYViiY8wGPFp80Nb4l34o9hQwY5FPYEzYfI+CUkOTAi4avy25+v0/hFGA2AQeMZzkCgYEAoCqdh9vUzFpmKl6SQfjlewAn6H0huxqKtLsVCjEzoq43PxxEBcd2i5qFTWTkggRNmP1xQ7T5fB3OKN7K9qfwawConQK6Fyg8TH6mtWC/l4GWYyFkqs2lNS/YiS2IBdjY60AEtaFYeEZGjdu+1yVjA7P7Bcc6W5MLcr1HFFjbmNMCgYEA3dsPSF/1cyhZjy96MLg6CtRsulyVWVwaQahqHGslIH+bs4iveKPqFIXMEDSkv3WT7MVid7kZupr7c3oa3Hqks8WbCGBQvhJIAoCUgUiEJYlQUGDCi86nu6kNZrnyceu5nEQ7dLinADrIxS+c6nlCvyPbp52WZGoDhkWR2YeZ2pkCgYB83dxkb5DuJEfkHkieIEIf65YxXWSvrtiWYAfeI0+jM3kkvY+WhdO4sKXeGZtAdcRdUrE73Npn1gDEwjSvqWv4r9HEXrzfkuG3rUGgxCV6I3AwJneJ5bs3xO5mu9WA4XbzqLxDMd98HSWiMMAOkTopR+aasaCghfn14PSKO2AwOwKBgQCZ4yW8qOCajX6s6fs2buTDJL4mQbObmAhPnsJkcsQrzJ3WVUf5YsxskUEtAT0HCN9SgadrHNmZaiRI6MAuuBH8R0CAoPefwLpK/JoIL+d1Vjzb/sZRE6Qnlp2Xtl4mrlSTLfB9QZRNM2LPXYBFMdTrXWe0F12E1JvNP3A/gDzVtQ==";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuHRMf8S5r4CSiWFQSicEr/5ZfRivxol4Sr0w9Mt9RuDVEq844FyaxHXkQdkZPEBqkp9hCAkZMJM8rwVGJAvCAHV07oh8h7sFH/rVkP6Fb/DxrBlzU9pgnpoibqFEPbX/JukgFzMXhrataNk0qREBYBxdWDibemwjdCDqOkRB3oZipUcf1uXWLoy1Fw+a5TPae9DUxCklpUT7xFAk3nQxZq6o8eK9OP+vr+LD2gqQ9wiFtcJuLt/+dRjWWS/7aZubIly+JkevYDN4X7Gw/flSLE3ZSZn0TIFUDtHI/gmn4WiQFurH4eWH+PwDLr4vvjWJgfAOuK7w1M0nd5EJcTyoTwIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://localhost:8080/p2p/loan/alipayNotify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://localhost:8080/p2p/loan/alipayBack";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
