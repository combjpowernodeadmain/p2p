package com.alipay.config;

import java.io.FileWriter;
import java.io.IOException;

/**
 * ClassName:AlipayConfig
 * Package:com.alipay.config
 * Description:
 *
 * @date:2019/6/21 10:56
 * @author:guoxin
 */
public class AlipayConfig {
    //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2016101000656745";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCW9Cd0MQNj6igAyKh/ZDm6X94GqKNn1t6oPOqSXFyE40cqxAzMLXPNONvRlKArtvrlSbof1UBF5o44XK+F2FVYDPKy9db9+X28na46SdEzXDSkbIc46CDU+XiX0ygOm9bGcrRwFj+Txsd6cEiBwHSlpesbUSRQTvJiDbpH1RBghgGmu/kJrCj7JGJTRDHXqbgSIpGWiM7eGU8nABRxzMwlISzY7eJKxTITz0KkMBhTdFA5/fdw4Y1v6K7PJ54SpjvYANIdoMCF1HUKaw8dnsMh18XyZDefdjOHS0pTIQSXJKqbYToA0utrn5O9XhRhJxIZKHI4EvwjOk6EMzQVRrFNAgMBAAECggEAOJC5EBMlwBUIdHK8oS//adVur3QV911PBh/9jmbJzIoWKvJYEVKe6DH8PSbCdl8U2UKtWixg71Cbzob8xtzv7hS8BU+WBomtEjbMCM+McwCEQXDhovNpCiWJiOr5QbAn5SkqcC4GGOwN59WXdfeWPlRUK2JWhzluHkwil7RBdMlvFOzrKcyxdp+isVjNV9/+CsA1FOTUrgHNHC0tjK2CcI3NOJ/G4koO91yp8C1tVoheiOm8pDOdWegGYYsHpEC+vBM77nuwLhU6V1HxnxaWLgKt10dAAErezFuomLlGyu1xr752mNUJeBW3AmDye6bKreK1GPPXz7qCKxCBb9sugQKBgQDFDsgzbeMAKlu6oDI+yTaMw4NM4/VhR83is7eRyASS7monzZX90waHj/2ctR+M4aosDwkri5rpnzlFIHuFX1WjNbXWatvazmqOKZD1e8nl+/VLo+CUQUHQIUffElO46Z8UJbZTENtf65JqUBBKyanPNe51/ReO/WbLZ+8TnH52PQKBgQDEGxGwUItvNSM1KvnHWchmLQL31o++CHo3lBiPvJ5Dsy22kNOXo9RrPtWzOfrzWraYUqYuV+Ydk3WYvfs0X7/liTEG0EYcLObX1zze+WoQaJIGSN+r4QVCUd5scAEmsLWN0ODwKOiH4bb7ryTqTAZhDGitB7SjK8PFxJHjDEzoUQKBgQCWitNxTsqzrqiDK60XADsXUKn3TNOCLQvh3DAqQ+cSpOceX1XuIpADcoJqfIiM5T1PCXGU/cMsJULoR4d1/b37wKWZF+u7/iOzVZfzok080L61uY1xVWVE7pfsmCCzzG6GQsVGMkOVxO8HHwFDUUrUDTyOkPq1d2tXi6A+mDiYdQKBgEwDXhDkt5dslnAWb2k5QPdx/ROKBH+Zk54VdovIheQiANciPhIqO70yRu1IzSAlCl/ysV2HTrgqH0wMX+HGN4DGc817T6G+OeByGrMnJFil3+w1tRVPjJVPXJLVu2x+1HDbBUWx5ag98oqOMhXrUBGwGKbxNLCG10TCU/YZH3pxAoGAGf/NAuF98PmUkmUXECkFhpxYjsKn0NUPbd29F60ljeVPInha2f4s+LClvPvnzkucIdn73KE06pJfEtC5OkltuT99NFsy5joW8m9Wwn9XgfMvoZwvmdfr0J9JWYBlI8TvfS0U4mYHf6+UO2R3u5viyamGu+5ClXAWuzHDMhHQ4Ok=";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsY9Zmk68nYyiqukoZpbrZTCmruX3Dp8egTXt+RvZv5K/i2qnSKy8I3FJCamj7SiS0tva+v7Jaldz9QmaPRA3cb0zreKCZJ0KVBLu7aadDVCTp4IWZWNP78FMpo46DtDGZ3GwtOqrcH5XBEPxrDp1pfrUywQD79AGHYXzib5xqWBVI/44qmrsoydMQm6Sbkp2cjos9VH5aNJ97ciSTbvuS0RCrVfZko+x5NZxYByuUR2dJmyc8DeezdI3cGyusyl1WXx1vui2vCbPylHb+4XB9l75VcnrUILtEsEFBsBtyiNSu+zyrIX5ntf7poM5tbMFpRVx6inIlvvh70fWLCn25wIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://localhost:9091/notify_url.jsp";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://localhost:9091/return_url.jsp";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
