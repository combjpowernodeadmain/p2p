package com.bjpowernode.xpath;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;

import java.util.List;

/**
 * ClassName:Test
 * Package:com.bjpowernode.xpath
 * Description:
 *
 * @date:2019/6/17 16:22
 * @author:guoxin
 */
public class Test {
    public static void main(String[] args) throws DocumentException {

        String xmlStr = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><bookstore><book><title lang=\"eng\">Harry Potter</title><price>29.99</price></book><book><title lang=\"eng\">Learning XML</title><price>39.95</price></book></bookstore>";

        //将xml格式的字符串转换为document对象
        Document document = DocumentHelper.parseText(xmlStr);

        //获取第二个book下的title的文本内容
        //xpath表达：//book[2]/title 、bookstore/book[2]/title 、//title[2] 、//title[@lang]
        Node node = document.selectSingleNode("//book[2]/title");


        //获取节点的文本内容
        String text = node.getText();

//        System.out.println(text);


        List<Node> nodes = document.selectNodes("//title");
        Node node1 = document.selectSingleNode("/bookstore/book[2]/title");
//        System.out.println(nodes.size());

        for (Node node2 : nodes) {
            String text1 = node2.getText();
//            System.out.println(text1);
        }


        String test1 = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><bookstore><a><b>33333</b></a><a><b>44444</b></a></bookstore>";
        Document document1 = DocumentHelper.parseText(test1);
        Node node2 = document1.selectSingleNode("//b[last()]");

        System.out.println(node2.getText());



    }
}
