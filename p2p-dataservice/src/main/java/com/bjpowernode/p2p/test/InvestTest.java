package com.bjpowernode.p2p.test;

import com.bjpowernode.p2p.service.loan.BidInfoService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ClassName:InvestTest
 * Package:com.bjpowernode.p2p.test
 * Description:
 *
 * @date:2019/6/18 16:14
 * @author:guoxin
 */
public class InvestTest {

    public static void main(String[] args) {
        //创建spring容器
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");

        //获取容器中指定的bean
        BidInfoService bidInfoService = (BidInfoService) applicationContext.getBean("bidInfoServiceImpl");

        //创建一个固定的线程池
        ExecutorService executorService = Executors.newFixedThreadPool(100);

        Map<String,Object> paramMap = new HashMap<String, Object>();
        paramMap.put("uid",1);
        paramMap.put("loanId",3);
        paramMap.put("bidMoney",1.0);

        for (int i = 0; i < 1000; i++) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    bidInfoService.invest(paramMap);
                }
            });
        }

        executorService.shutdown();
    }
}
