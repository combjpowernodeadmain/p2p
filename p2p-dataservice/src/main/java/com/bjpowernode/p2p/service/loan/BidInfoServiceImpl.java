package com.bjpowernode.p2p.service.loan;

import com.bjpowernode.p2p.common.constant.Constants;
import com.bjpowernode.p2p.mapper.loan.BidInfoMapper;
import com.bjpowernode.p2p.mapper.loan.LoanInfoMapper;
import com.bjpowernode.p2p.mapper.user.FinanceAccountMapper;
import com.bjpowernode.p2p.model.loan.BidInfo;
import com.bjpowernode.p2p.model.loan.LoanInfo;
import com.bjpowernode.p2p.model.vo.BidUser;
import com.bjpowernode.p2p.model.vo.PaginationVO;
import com.bjpowernode.p2p.model.vo.RecentBidInfoVO;
import com.bjpowernode.p2p.model.vo.ResultObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * ClassName:BidInfoServiceImpl
 * Package:com.bjpowernode.p2p.service.loan
 * Description:
 *
 * @date:2019/6/13 16:19
 * @author:guoxin
 */
@Service("bidInfoServiceImpl")
public class BidInfoServiceImpl implements BidInfoService {

    @Autowired
    private BidInfoMapper bidInfoMapper;

    @Autowired
    private LoanInfoMapper loanInfoMapper;

    @Autowired
    private FinanceAccountMapper financeAccountMapper;

    @Autowired
    private RedisTemplate<Object,Object> redisTemplate;


    @Override
    public Double queryAllBidMoney() {

        //从Redis中获取累计投资金额
//        Double allBidMoney = (Double) redisTemplate.opsForValue().get(Constants.ALL_BID_MONEY);

        //首先获取操作某种数据类型的对象
        BoundValueOperations<Object, Object> boundValueOperations = redisTemplate.boundValueOps(Constants.ALL_BID_MONEY);

        //再从该对象中获取该值
        Double allBidMoney = (Double) boundValueOperations.get();

        //判断是否有值
        if (null == allBidMoney) {

            //设置同步代码块
            synchronized (this) {

                //从redis中获取该值
                allBidMoney  = (Double) boundValueOperations.get();

                //双重检测
                if (null == allBidMoney) {

                    //去数据库查询
                    allBidMoney = bidInfoMapper.selectAllBidMoney();

                    //将该值存放到redis缓存中
                    boundValueOperations.set(allBidMoney,15,TimeUnit.MINUTES);
                }
            }

        }




        return allBidMoney;
    }

    @Override
    public List<BidInfo> queryRecentlyBidInfoListByLoanId(Map<String,Object> paramMap) {
        return bidInfoMapper.selectRecentlyBidInfoListByLoanId(paramMap);
    }

    @Override
    public List<RecentBidInfoVO> queryRecentlyBidInfoListByUid(Map<String, Object> paramMap) {
        return bidInfoMapper.selectRecentlyBidInfoListByUid(paramMap);
    }

    @Override
    public PaginationVO<RecentBidInfoVO> queryBidInfoByPage(Map<String, Object> paramMap) {
        PaginationVO<RecentBidInfoVO> paginationVO = new PaginationVO<>();

        Long total = bidInfoMapper.selectTotal(paramMap);

        paginationVO.setTotal(total);

        List<RecentBidInfoVO> recentBidInfoVOList = bidInfoMapper.selectRecentlyBidInfoListByUid(paramMap);

        paginationVO.setDataList(recentBidInfoVOList);

        return paginationVO;
    }

    @Override
    public ResultObject invest(Map<String, Object> paramMap) {
        ResultObject resultObject = new ResultObject();
        resultObject.setErrorCode(Constants.SUCCESS);

        Integer loanId = (Integer) paramMap.get("loanId");
        Integer uid = (Integer) paramMap.get("uid");
        Double bidMoney = (Double) paramMap.get("bidMoney");
        String phone = (String) paramMap.get("phone");

        //更新产品的剩余可投金额
        //超卖：实际销售的数量超过了库存数量
        //使用数据库乐观锁机制来解决超卖
        LoanInfo loanInfo = loanInfoMapper.selectByPrimaryKey(loanId);
        paramMap.put("version",loanInfo.getVersion());

        int updateLeftProductMoneyCount = loanInfoMapper.updateLeftProductMoneyByLoanId(paramMap);

        if (updateLeftProductMoneyCount > 0) {

            //更新用户帐户余额
            int updateFinanceCount = financeAccountMapper.updateFinanceAccountByBid(paramMap);

            if (updateFinanceCount > 0) {
                //新增投资记录
                BidInfo bidInfo = new BidInfo();
                bidInfo.setUid(uid);
                bidInfo.setLoanId(loanId);
                bidInfo.setBidMoney(bidMoney);
                bidInfo.setBidTime(new Date());
                bidInfo.setBidStatus(1);
                int insertBidInfoCount = bidInfoMapper.insertSelective(bidInfo);

                if (insertBidInfoCount > 0) {
                    //再次查询产品的详情
                    LoanInfo loanDetail = loanInfoMapper.selectByPrimaryKey(loanId);

                    //判断产品是否满标
                    if (0 == loanDetail.getLeftProductMoney()) {

                        //已满标 -> 更新产品的状态及满标时间
                        LoanInfo updateLoanInfo = new LoanInfo();
                        updateLoanInfo.setId(loanDetail.getId());
                        updateLoanInfo.setProductFullTime(new Date());
                        updateLoanInfo.setProductStatus(1);
                        int i = loanInfoMapper.updateByPrimaryKeySelective(updateLoanInfo);
                        if (i <= 0) {
                            resultObject.setErrorCode(Constants.FAIL);
                        }
                    }

                    //将用户的投资信息存放到redis缓存中
                    redisTemplate.opsForZSet().incrementScore(Constants.INVEST_TOP,phone,bidMoney);
                    

                } else {
                    resultObject.setErrorCode(Constants.FAIL);
                }
            } else {
                resultObject.setErrorCode(Constants.FAIL);
            }

        } else {
            resultObject.setErrorCode(Constants.FAIL);
        }

        return resultObject;
    }

    @Override
    public List<BidUser> queryBidUserTop() {
        List<BidUser> bidUserList = new ArrayList<BidUser>();

        //从Redis中获取排行榜
        Set<ZSetOperations.TypedTuple<Object>> typedTuples = redisTemplate.opsForZSet().reverseRangeWithScores(Constants.INVEST_TOP, 0, 5);

        //获取迭代器
        Iterator<ZSetOperations.TypedTuple<Object>> iterator = typedTuples.iterator();

        //循环遍历
        while (iterator.hasNext()) {

            ZSetOperations.TypedTuple<Object> next = iterator.next();
            Double score = next.getScore();
            String phone = (String) next.getValue();

            BidUser bidUser = new BidUser();
            bidUser.setPhone(phone);
            bidUser.setScore(score);

            bidUserList.add(bidUser);

        }



        return bidUserList;
    }
}
