package com.bjpowernode.p2p.web;

import com.alibaba.fastjson.JSONObject;
import com.bjpowernode.http.HttpClientUtils;
import com.bjpowernode.p2p.common.constant.Constants;
import com.bjpowernode.p2p.model.user.FinanceAccount;
import com.bjpowernode.p2p.model.user.User;
import com.bjpowernode.p2p.model.vo.RecentBidInfoVO;
import com.bjpowernode.p2p.model.vo.ResultObject;
import com.bjpowernode.p2p.service.loan.BidInfoService;
import com.bjpowernode.p2p.service.loan.LoanInfoService;
import com.bjpowernode.p2p.service.loan.RedisService;
import com.bjpowernode.p2p.service.user.FinanceAccountService;
import com.bjpowernode.p2p.service.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName:UserController
 * Package:com.bjpowernode.p2p.web
 * Description:
 *
 * @date:2019/6/15 10:24
 * @author:guoxin
 */
@Controller
//@RestController //等同于 类上加@Controller 和方法上加@ResponseBody
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private LoanInfoService loanInfoService;

    @Autowired
    private BidInfoService bidInfoService;

    @Autowired
    private FinanceAccountService financeAccountService;

    @Resource
    private RedisService redisService;

    /**
     * 接口地址：http://localhost:8080/p2p/loan/checkPhone
     * @param request
     * @param phone 必填
     * @return 响应JSON格式的字符串 {"errorMessage":"OK"|"错误消息"}
     */
    @RequestMapping(value = "/loan/checkPhone")
    @ResponseBody
    public Object checkPhone(HttpServletRequest request,
                                           @RequestParam (value = "phone",required = true) String phone) {
        Map<String,Object> retMap = new HashMap<String,Object>();

        //根据手机号码查询用户是否存在(手机号码) -> 返回int|boolean
        //根据手机号码查询用户信息(手机号码) -> 返回User对象
        User user = userService.queryUserByPhone(phone);

        //判断用户是否为空
        if (null != user) {
            retMap.put(Constants.ERROR_MESSAGE,"该手机号码已存在，请更换手机号码");
            return retMap;
        }

        retMap.put(Constants.ERROR_MESSAGE,Constants.OK);

        return retMap;
    }

//    @RequestMapping(value = "/loan/checkCaptcha",method = RequestMethod.POST) //等同于 @PostMapping(value="/loan/checkCaptcha")
    @PostMapping(value = "/loan/checkCaptcha")
    @ResponseBody
    public Map<String,Object> checkCaptcha(HttpServletRequest request,
                               @RequestParam (value = "captcha",required = true) String captcha) {
        Map<String,Object> retMap = new HashMap<String, Object>();

        //从session中获取图形验证码
        String sessionCaptcha = (String) request.getSession().getAttribute(Constants.CAPTCHA);

        //判断用户输入图形验证码和session中的是否一致
        if (!StringUtils.equalsIgnoreCase(sessionCaptcha, captcha)) {
            retMap.put(Constants.ERROR_MESSAGE,"请输入正确的图形验证码");
            return retMap;
        }

        retMap.put(Constants.ERROR_MESSAGE,Constants.OK);


        return retMap;
    }

//    @RequestMapping(value = "/loan/register", method = RequestMethod.GET)//等同于 @GetMapping(value="/loan/register")
    @GetMapping(value = "/loan/register")
    @ResponseBody
    public Map<String, Object> register(HttpServletRequest request,
                                        @RequestParam (value = "phone",required = true) String phone,
                                        @RequestParam (value = "loginPassword",required = true) String loginPassword) {

        Map<String,Object> retMap = new HashMap<String, Object>();

        //用户注册【1.新增用户 2.开立帐户】(手机号,登录密码) -> 返回int|boolean|业务处理结果ResultObject
        ResultObject resultObject = userService.register(phone,loginPassword);

        if (StringUtils.equals(resultObject.getErrorCode(), Constants.SUCCESS)) {

            //根据手机号查询用户信息
            User user = userService.queryUserByPhone(phone);

            //将用户的信息存放到session中
            request.getSession().setAttribute(Constants.SESSION_USER,user);

            retMap.put(Constants.ERROR_MESSAGE,Constants.OK);

        } else {
            retMap.put(Constants.ERROR_MESSAGE,"注册失败");
            return retMap;
        }


        return retMap;
    }


    @RequestMapping(value = "/loan/financeAccount")
    @ResponseBody
    public FinanceAccount financeAccount(HttpServletRequest request) {
        //从session中获取用户的信息
        User sessionUser = (User) request.getSession().getAttribute(Constants.SESSION_USER);

        //根据用户的标识获取帐户信息
        FinanceAccount financeAccount = financeAccountService.queryFinanceAccountByUid(sessionUser.getId());

        return financeAccount;
    }


    @RequestMapping(value = "/loan/test")
    public @ResponseBody FinanceAccount test(HttpServletRequest request) {
        //从session中获取用户的信息
        User sessionUser = (User) request.getSession().getAttribute(Constants.SESSION_USER);

        //根据用户的标识获取帐户信息
        FinanceAccount financeAccount = financeAccountService.queryFinanceAccountByUid(sessionUser.getId());

        return financeAccount;
    }

    @PostMapping(value = "/loan/verifyRealName")
    @ResponseBody
    public Map<String,Object> verifyRealName(HttpServletRequest request,
                                             @RequestParam (value = "idCard",required = true) String idCard,
                                             @RequestParam (value = "realName",required = true) String realName) throws Exception {
        Map<String,Object> retMap = new HashMap<String, Object>();

        //准备请求参数
        Map<String,Object> paramMap = new HashMap<String,Object>();

        //您申请的key
        paramMap.put("appkey","0bd1f286242a3f23d6144");

        //用户的身份证号码
        paramMap.put("cardNo",idCard);

        //用户真实姓名
        paramMap.put("realName",realName);

        //调用互联网实名认证接口，京东万象平台的身份证二要素接口进行验证
        String jsonString = HttpClientUtils.doPost("https://way.jd.com/youhuoBeijing/test", paramMap);
//        String jsonString = "{\"code\":\"10000\",\"charge\":false,\"remain\":1305,\"msg\":\"查询成功\",\"result\":{\"error_code\":0,\"reason\":\"成功\",\"result\":{\"realname\":\"乐天磊\",\"idcard\":\"350721197702134399\",\"isok\":true}}}";

        //使用fastjson来解析json格式的字符串
        //将json格式的字符串转换为JSON对象
        JSONObject jsonObject = JSONObject.parseObject(jsonString);

        //获取通信标识code
        String code = jsonObject.getString("code");

        //判断通信是否成功
        if (StringUtils.equals("10000", code)) {

            //获取是否匹配isok
            Boolean isok = jsonObject.getJSONObject("result").getJSONObject("result").getBoolean("isok");

            //获取用户输入的真实姓名和身份证号码是否一致
            if (isok) {
                //从session中获取用户信息
                User sessinoUser = (User) request.getSession().getAttribute(Constants.SESSION_USER);

                //实名认证 -> 本质：更新用户信息
                User user = new User();
                user.setId(sessinoUser.getId());
                user.setName(realName);
                user.setIdCard(idCard);
                int modifyUserCount = userService.modifyUserById(user);

                if (modifyUserCount > 0) {

                    retMap.put(Constants.ERROR_MESSAGE,Constants.OK);
                    sessinoUser.setName(realName);
                    sessinoUser.setIdCard(idCard);
                    request.getSession().setAttribute(Constants.SESSION_USER,sessinoUser);

                } else {
                    retMap.put(Constants.ERROR_MESSAGE,"系统繁忙，请稍后重试");
                    return retMap;
                }

            } else {
                //真实姓名和身份证号码不一致
                retMap.put(Constants.ERROR_MESSAGE,"真实姓名和身份证号码不一致");
                return retMap;
            }


        } else {
            //通信失败
            retMap.put(Constants.ERROR_MESSAGE,"通信异常");
            return retMap;
        }




        return retMap;
    }

    @RequestMapping(value = "/loan/logout")
    public String logout(HttpServletRequest request) {

        //让session失效||清除指定的session值
        request.getSession().invalidate();
        request.getSession().removeAttribute(Constants.SESSION_USER);

        return "redirect:/index";
    }


    @RequestMapping(value = "/loan/loadStat")
    public @ResponseBody Object loadStat() {
        Map<String,Object> retMap = new HashMap<String,Object>();

        //历史平均年化收益率
        Double historyAverageRate = loanInfoService.queryHistoryAverageRate();

        //平台注册总人数
        Long allUserCount = userService.queryAllUserCount();

        //平台累计投资金额
        Double allBidMoney = bidInfoService.queryAllBidMoney();

        retMap.put(Constants.HISTORY_AVERAGE_RATE,historyAverageRate);
        retMap.put(Constants.ALL_USER_COUNT,allUserCount);
        retMap.put(Constants.ALL_BID_MONEY,allBidMoney);

        return retMap;
    }


    @RequestMapping(value = "/loan/login")
    public @ResponseBody Object login(HttpServletRequest request,
                                      @RequestParam (value = "phone",required = true) String phone,
                                      @RequestParam (value = "loginPassword",required = true) String loginPassword,
                                      @RequestParam (value = "messageCode",required = true) String messageCode) {
        Map<String,Object> retMap = new HashMap<String,Object>();

        //从redis中获取该手机号码所对应的值
        String redisMessageCode = redisService.get(phone);

        //验证短信验证码
        if (StringUtils.equals(messageCode, redisMessageCode)) {

            //用户登录【1.根据手机号和用户密码查询用户信息 2.更新最近登录时间】(手机，密码) -> 返回User
            User user = userService.login(loginPassword,phone);

            //判断用户是否存在
            if (null == user) {
                retMap.put(Constants.ERROR_MESSAGE,"用户名或密码有误");
                return retMap;
            }

            retMap.put(Constants.ERROR_MESSAGE,Constants.OK);

            //将用户的信息存放到session中
            request.getSession().setAttribute(Constants.SESSION_USER,user);

        } else {
            retMap.put(Constants.ERROR_MESSAGE,"请输入正确的短信验证码");
            return retMap;
        }




        return retMap;
    }


    @RequestMapping(value = "/loan/messageCode")
    public @ResponseBody Object messageCode(HttpServletRequest request,
                                            @RequestParam (value = "phone",required = true) String phone) throws Exception {
        Map<String,Object> retMap = new HashMap<String, Object>();

        //生成一个随机数字
        String messageCode = getRandomNumber(6);

        //准备短信验证码的短信内容 = 短信签名 + 短信正文(是需要审核)
        String content = "【凯信通】您的验证码是：" + messageCode;

        //准备接口参数
        Map<String,Object> paramMap = new HashMap<String, Object>();

        //申请的appkey
        paramMap.put("appkey","29feced2020768bf170ecb24");

        //手机号码
        paramMap.put("mobile",phone);

        //短信内容
        paramMap.put("content",content);

        //调用互联网接口，调用：京东万象的106短信接口
//        String jsonString = HttpClientUtils.doPost("https://way.jd.com/kaixintong/kaixintong", paramMap);
        String jsonString = "{\n" +
                "    \"code\": \"10000\",\n" +
                "    \"charge\": false,\n" +
                "    \"remain\": 0,\n" +
                "    \"msg\": \"查询成功\",\n" +
                "    \"result\": \"<?xml version=\\\"1.0\\\" encoding=\\\"utf-8\\\" ?><returnsms>\\n <returnstatus>Success</returnstatus>\\n <message>ok</message>\\n <remainpoint>-883104</remainpoint>\\n <taskID>92959771</taskID>\\n <successCounts>1</successCounts></returnsms>\"\n" +
                "}";

        //使用fastjson来解析json格式的字符串
        //将json格式的字符串转换为JSON对象
        JSONObject jsonObject = JSONObject.parseObject(jsonString);

        //获取到通信标识code
        String code = jsonObject.getString("code");

        //判断通信是否成功
        if (StringUtils.equals("10000", code)) {

            //获取result，该值是xml格式的字符串
            String resultXml = jsonObject.getString("result");

            //使用dom4j+xpath解析xml格式的字符串
            //将xml格式的字符串转换为Document对象
            Document document = DocumentHelper.parseText(resultXml);

            //获取returnstatus节点的文本内容
            //该节点的xpath路径：/returnsms/returnstatus 或者 //returnstatus
            Node node = document.selectSingleNode("//returnstatus");

            //获取该节点的文本内容
            String returnstatus = node.getText();

            //判断短信是否发送成功
            if (StringUtils.equals("Success", returnstatus)) {

                //将生成的随机数字存放到redis缓存中
                redisService.put(phone,messageCode);

                retMap.put("messageCode",messageCode);

                retMap.put(Constants.ERROR_MESSAGE,Constants.OK);


            } else {
                retMap.put(Constants.ERROR_MESSAGE,"通信异常，请稍后重试");
                return retMap;
            }


        } else {
            retMap.put(Constants.ERROR_MESSAGE,"通信异常，请稍后重试");
            return retMap;
        }


        return retMap;
    }

    private String getRandomNumber(int count) {

        String[] arr = {"0","1","2","3","4","5","6","7","8","9"};

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < count; i++) {
            int index = (int) Math.round(Math.random()*9);
            sb.append(arr[index]);
        }


        return sb.toString();
    }

    @RequestMapping(value = "/loan/myCenter")
    public String myCenter(HttpServletRequest request, Model model) {

        //从session中获取用户信息
        User sessionUser = (User) request.getSession().getAttribute(Constants.SESSION_USER);

        //根据用户标识获取帐户资金信息
        FinanceAccount financeAccount = financeAccountService.queryFinanceAccountByUid(sessionUser.getId());
        model.addAttribute("financeAccount",financeAccount);

        //将以下查询看作是一个分页
        //准备参数
        Map<String,Object> paramMap = new HashMap<String, Object>();
        paramMap.put("uid",sessionUser.getId());
        paramMap.put("currentPage",0);
        paramMap.put("pageSize",5);

        //根据用户标识获取最近投资记录,显示第1页，每页显示5条
        List<RecentBidInfoVO> recentBidInfoVOList = bidInfoService.queryRecentlyBidInfoListByUid(paramMap);
        model.addAttribute("recentBidInfoVOList",recentBidInfoVOList);

        //根据用户标识获取最近收益记录,显示第1页，每页显示5条

        //根据用户标识获取最近充值记录,显示第1页，每页显示5条


        return "myCenter";
    }


}
