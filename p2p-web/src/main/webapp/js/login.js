
//登录后返回页面的URL
var referrer = "";

// alert(!referrer);//true
// alert(referrer);//""
referrer = document.referrer;//跳转至当前页面之前页面的URL
// alert(referrer);//url
// alert(!referrer);//false

if (!referrer) {
	try {
		if (window.opener) {                
			// IE下如果跨域则抛出权限异常，Safari和Chrome下window.opener.location没有任何属性              
			referrer = window.opener.location.href;
		}  
	} catch (e) {
	}
}

//按键盘Enter键即可登录
$(document).keyup(function(event){
	if(event.keyCode == 13){
		login();
	}
});



$(function () {
	loadStat();

	$("#dateBtn1").on("click",function () {
		var phone = $.trim($("#phone").val());

		if (checkPhone() && checkLoginPassword()) {

			$.ajax({
				url:"loan/messageCode",
				type:"post",
				data:"phone="+phone,
				success:function (jsonObject) {
					if (jsonObject.errorMessage == "OK") {
						alert(jsonObject.messageCode);
						$.leftTime(60,function (d) {
							if (d.status) {
								$("#dateBtn1").addClass("on");
								$("#dateBtn1").html((d.s == "00"?"60":d.s) + "秒后重新获取");
							} else {
								$("#dateBtn1").removeClass("on");
								$("#dateBtn1").html("获取短信验证码");
							}
						});

					} else {
						$("#showId").html("短信异常，请稍后重试");
					}
				},
				error:function () {
					$("#showId").html("短信异常，请稍后重试");
				}
			});


		}



	});
});

function loadStat() {
	$.ajax({
		url:"loan/loadStat",
		type:"get",
		success:function (jsonObject) {
			$(".historyAverageRate").html(jsonObject.historyAverageRate);
			$("#allUserCount").html(jsonObject.allUserCount);
			$("#allBidMoney").html(jsonObject.allBidMoney);
		}
	});
}


function checkPhone() {
	var phone = $.trim($("#phone").val());

	if ("" == phone) {
		$("#showId").html("请输入手机号码");
		return false;
	} else if (!/^1[1-9]\d{9}$/.test(phone)) {
		$("#showId").html("请输入正确的手机号码");
		return false;
	} else {
		$("#showId").html("");
	}
	return true;
}


function checkLoginPassword() {
	var loginPassword = $.trim($("#loginPassword").val());

	if ("" == loginPassword) {
		$("#showId").html("请输入登录密码");
		return false;
	} else {
		$("#showId").html("");
	}

	return true;
}

function checkCaptcha() {
	var captcha = $.trim($("#captcha").val());
	var flag = false;

	if ("" == captcha) {
		$("#showId").html("请输入图形验证码");
		return false;
	} else {
		$.ajax({
			url:"loan/checkCaptcha",
			type:"post",
			data:"captcha="+captcha,
			async:false,
			success:function (jsonObject) {
				if (jsonObject.errorMessage == "OK") {
					$("#showId").html("");
					flag = true;
				} else {
					$("#showId").html(jsonObject.errorMessage);
					flag = false;
				}
			},
			error:function () {
				$("#showId").html("系统繁忙，请稍后重试");
				flag = false;
			}
		});
	}

	if (!flag) {
		return false;
	}
	return true;
}

function checkMessageCode() {
	var messageCode = $.trim($("#messageCode").val());

	if ("" == messageCode) {
		$("#showId").html("请输入短信验证码");
		return false;
	} else {
		$("#showId").html("");
	}
	return true;
}


function login() {
	var phone = $.trim($("#phone").val());
	var loginPassword = $.trim($("#loginPassword").val());
	var messageCode = $.trim($("#messageCode").val());

	if (checkPhone() && checkLoginPassword() && checkMessageCode()) {

		$("#loginPassword").val($.md5(loginPassword));

		$.ajax({
			url:"loan/login",
			type:"post",
			data:"phone="+phone+"&loginPassword="+$.md5(loginPassword)+"&messageCode="+messageCode,
			success:function (jsonObject) {
				if (jsonObject.errorMessage == "OK") {
					if ("" == referrer) {
						window.location.href = "index";
					} else {
						window.location.href = referrer;
					}
				} else {
					$("#showId").html(jsonObject.errorMessage);
				}
			},
			error:function () {
				$("#showId").html("系统繁忙，请稍后重试");

			}
		});


	}
}


















