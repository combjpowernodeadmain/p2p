package com.bjpowernode.p2p.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * ClassName:ResultObject
 * Package:com.bjpowernode.p2p.model.vo
 * Description:
 *
 * @date:2019/6/15 12:10
 * @author:guoxin
 */
@Data
public class ResultObject implements Serializable {

    /**
     * 错误码：SUCCESS|FAIL
     */
    private String errorCode;

}
