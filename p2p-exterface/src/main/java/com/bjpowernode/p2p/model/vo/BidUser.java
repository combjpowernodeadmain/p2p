package com.bjpowernode.p2p.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * ClassName:BidUser
 * Package:com.bjpowernode.p2p.model.vo
 * Description:
 *
 * @date:2019/6/20 9:15
 * @author:guoxin
 */
@Data
public class BidUser implements Serializable {

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 分数：累计投资金额
     */
    private Double score;

}
