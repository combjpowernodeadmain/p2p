package com.bjpowernode.p2p.service.loan;

/**
 * ClassName:IncomeRecordService
 * Package:com.bjpowernode.p2p.service.loan
 * Description:
 *
 * @date:2019/6/20 10:36
 * @author:guoxin
 */
public interface IncomeRecordService {

    /**
     * 生成收益计划
     */
    void generateIncomePlan();

    /**
     * 返还收益计划
     */
    void generateIncomeBack();
}
