package com.bjpowernode.p2p.service.loan;

/**
 * ClassName:RedisService
 * Package:com.bjpowernode.p2p.service.loan
 * Description:
 *
 * @date:2019/6/18 10:30
 * @author:guoxin
 */
public interface RedisService {

    /**
     * 将指定的值存放以redis中的key
     * @param key
     * @param value
     */
    void put(String key, String value);

    /**
     * 从redis中获取指定key的值
     * @param key
     * @return
     */
    String get(String key);

    /**
     * 从redis中获取唯一数字
     * @return
     */
    Long getOnlyNumber();

}
