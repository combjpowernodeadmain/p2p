package com.bjpowernode.p2p.service.loan;

import com.bjpowernode.p2p.model.loan.BidInfo;
import com.bjpowernode.p2p.model.vo.BidUser;
import com.bjpowernode.p2p.model.vo.PaginationVO;
import com.bjpowernode.p2p.model.vo.RecentBidInfoVO;
import com.bjpowernode.p2p.model.vo.ResultObject;

import java.util.List;
import java.util.Map;

/**
 * ClassName:BidInfoService
 * Package:com.bjpowernode.p2p.service.loan
 * Description:
 *
 * @date:2019/6/13 16:18
 * @author:guoxin
 */
public interface BidInfoService {

    /**
     * 获取平台累计投资金额
     * @return
     */
    Double queryAllBidMoney();

    /**
     * 根据产品标识获取产品的最近投资记录
     * @return
     */
    List<BidInfo> queryRecentlyBidInfoListByLoanId(Map<String,Object> paramMap);

    /**
     * 根据用户标识获取最近投资记录
     * @param paramMap
     * @return
     */
    List<RecentBidInfoVO> queryRecentlyBidInfoListByUid(Map<String, Object> paramMap);

    /**
     * 根据用户标识分页查询投资记录
     * @param paramMap
     * @return
     */
    PaginationVO<RecentBidInfoVO> queryBidInfoByPage(Map<String, Object> paramMap);

    /**
     * 用户投资
     * @param paramMap
     * @return
     */
    ResultObject invest(Map<String, Object> paramMap);

    /**
     * 获取用户投资排行榜
     * @return
     */
    List<BidUser> queryBidUserTop();

}
